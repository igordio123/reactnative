/**
 * @format
 */
//import React from 'react'
//import {AppRegistry} from 'react-native';
//import App from './App';
//import {Provider} from 'react-redux';
//import configureStore from './src/store/configureStore'
//import {name as appName} from './app.json';
//
//
//AppRegistry.registerComponent(appName, () => App);
import { Navigation } from "react-native-navigation";
import AuthScreen from "./src/screens/Auth/Auth";

Navigation.registerComponent(`navigation.playground.WelcomeScreen`, () => AuthScreen);

Navigation.events().registerAppLaunchedListener(() => {
  Navigation.setRoot({
    root: {
      component: {
        name: "navigation.playground.WelcomeScreen",
      }
    }
  });
});