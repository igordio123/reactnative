// import React, {Component} from 'react';
// import {Platform, StyleSheet, Text, View, TextInput, Button, FlatList} from 'react-native';
// import {connect} from 'react-redux'
// import PlaceImage from './src/assets/download.jpeg'
// import {addPlace,deselectPlace,deletePlace,selectPlace} from './src/store/actions/index'
// import PlacesList from './src/components/PlacesList/PlacesList'
// import PlaceInput from './src/components/PlaceInput/PlaceInput'
// import PlaceDetail from './src/components/PlaceDetail/PlaceDetail'
//
// class App extends Component {
//     state = {
//         value: '',
//     };
//
//     constructor(props) {
//         super(props);
//         console.log(this.props)
//     }
//
//
//     placeNameChangeHandler = (text) => {
//         this.setState({value: text});
//     };
//     placeSubmitHandler = () => {
//        this.props.ooAddPlace(this.state.value)
//
//     };
//     onItemDeleted = () => {
//         this.props.onDelete()
//     };
//     onModalClosed = () => {
//         this.props.deselectPlace()
//     };
//
//     selectedItemHandler = (key) => {
//         this.props.onSelectedPlace(key)
//     };
//
//
//     render() {
//         return (
//             <View style={styles.container}>
//                 <PlaceDetail selectedPlace={this.props.selectedPlace} onItemDeleted={this.onItemDeleted}
//                              onModalClosed={this.onModalClosed}/>
//                 <PlaceInput value={this.state.value}
//                             placeNameChangeHandler={this.placeNameChangeHandler}
//                             placeSubmitHandler={this.placeSubmitHandler}
//                 />
//                 <PlacesList places={this.props.places} onItemSelected={this.selectedItemHandler}/>
//             </View>
//         );
//     }
// }
//
// const styles = StyleSheet.create({
//     container: {
//         flex: 1,
//         padding: 20,
//         justifyContent: 'flex-start',
//         alignItems: 'center',
//         backgroundColor: '#f5f2f0',
//     },
//     wrapper: {
//         width: '100%',
//         flexDirection: 'row',
//         alignItems: 'center',
//         justifyContent: 'space-between'
//     },
//     welcome: {
//         fontSize: 20,
//         textAlign: 'center',
//         margin: 10,
//     },
//     listContainer: {
//         width: '100%',
//
//     }
// });
// const mapStateToProps = state => {
//     return {
//         places : state.places.places,
//         selectedPlace : state.places.selectedPlace
//     }
// };
// const mapDispatchToProps = dispatch => {
//     return {
//         ooAddPlace: (name) => dispatch(addPlace(name)),
//         onDelete : () => dispatch(deletePlace()),
//         onSelectedPlace : (key) => dispatch(selectPlace(key)),
//         deselectPlace : () => dispatch(deselectPlace())
//     }
// };
// export default connect(mapStateToProps,mapDispatchToProps)(App)
//
// import { Navigation } from "react-native-navigation";
// import App from "./App";
//
// Navigation.registerComponent(`navigation.playground.WelcomeScreen`, () => App);
//
// Navigation.events().registerAppLaunchedListener(() => {
//   Navigation.setRoot({
//     root: {
//       component: {
//         name: "navigation.playground.WelcomeScreen"
//       }
//     }
//   });
// });