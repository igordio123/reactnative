import React from 'react'
import {Modal , View , Image , Text,Button,StyleSheet,TouchableOpacity} from 'react-native'
import Icon from 'react-native-vector-icons/Feather'

const placeDetail = (props) => {
    let modalContent = null;
    if(props.selectedPlace){
        modalContent=(
            <View style={styles.modalContent}>

                <Image style={styles.placeImage} source={props.selectedPlace.image ? props.selectedPlace.image : null}/>
                <Text style={styles.placeContent}>
                    {props.selectedPlace.value}
                </Text>
            </View>
        )
    }
  return (
      <Modal onRequestClose={props.onModalClosed}  visible={props.selectedPlace !== null} animationType='slide'>
          <View style={styles.wrapper} >
              {modalContent}
              <View style={styles.wrapTouchable}>
                  <TouchableOpacity  onPress={props.onItemDeleted}>
                      <Icon size={40} name={'trash'} color='red'/>
                  </TouchableOpacity>
                  <TouchableOpacity  onPress={props.onModalClosed}>
                      <Icon size={40} name={'x-circle'} color='black' />
                  </TouchableOpacity>
              </View>
          </View>
      </Modal>
  )

};
const styles = StyleSheet.create({
   wrapper:{
       flex:1,
       flexDirection: 'column'
   },
   modalContent:{
       flexDirection:'column',
       justifyContent:'center',
       alignItems:'center'
   },
   placeImage:{
       width:'100%'
   },
   placeContent:{
       fontFamily: 'Cochin',
       fontSize: 20,
       fontWeight: 'bold',

   },
   wrapTouchable:{
       justifyContent: 'center',
       alignItems:'center'
   }
});

export default placeDetail