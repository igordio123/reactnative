import React from 'react'
import {ScrollView, Text, StyleSheet,FlatList} from 'react-native'
import ListItem from "./ListItem/ListItem";

const placesList = (props) => {
    return (
        <FlatList style={styles.listContainer}
                  data = {props.places}
                  renderItem={(info)=>(
                      <ListItem content={info.item.value} image = {info.item.image} onItemSelected = {()=>props.onItemSelected(info.item.key)}/>
                  )}

        />
    )
};
const styles = StyleSheet.create({
    listContainer:{
        width:'100%',

    }
});
export default placesList