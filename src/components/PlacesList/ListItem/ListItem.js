import React from 'react'
import {View, Text, StyleSheet,TouchableOpacity , Image} from 'react-native'

const listItem = (props) => {

   return(
       <TouchableOpacity onPress = {props.onItemSelected}>
       <View style={styles.listItem} >
           <Image resizeMode='contain' style={styles.placeImage} source={props.image}/>
           <Text>{props.content}</Text>
       </View>
       </TouchableOpacity>
   )

};

const styles = StyleSheet.create({
    listItem:{
        width:'100%',
        marginTop:10,
        padding:10,
        borderRadius:5,
        backgroundColor:'#22474e',
        flexDirection:'row',
        alignItems:'center'
    },
    placeImage:{
        height:50,
        width: 50,
    }
});

export default listItem