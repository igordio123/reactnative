import React from 'react'
import {View, Text, StyleSheet, TouchableOpacity, TextInput, Button} from 'react-native'

const placeInput = (props) =>{

    return(
        <View style={styles.wrapper}>
        <TextInput style={{ borderColor: 'gray', borderWidth: 1,width:'70%'}}
                   placeholder='Enter your text'
                   value={props.value}
                   onChangeText={props.placeNameChangeHandler}  />
        <Button title='ADD' style={{width:'30%'}} onPress={props.placeSubmitHandler}/>
    </View>
    )

};

const styles = StyleSheet.create({
    wrapper:{
        width:'100%',
        flexDirection:'row',
        alignItems: 'center',
        justifyContent:'space-between'
    },
    welcome: {
        fontSize: 20,
        textAlign: 'center',
        margin: 10,
    },
    listContainer:{
        width:'100%',

    }
});

export default placeInput