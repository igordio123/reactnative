import {ADD_PLACE,DELETE_PLACE,DESELECT_PLACE,SELECT_PLACE} from './actionTypes'
import PlaceImage from "../../assets/download.jpeg";


export const addPlace = (placeName) => {

    return {
        type : ADD_PLACE,
        payload : {
            value:placeName,
            key:`${new Date().getTime()}`,
            image:PlaceImage
        }
    }

};

export const deletePlace = () => {

    return {
        type: DELETE_PLACE,
    }
};

export const selectPlace = (key) => {

    return {
        type:SELECT_PLACE,
        payload:key
    }

};

export const deselectPlace = () => {

    return {
        type:DESELECT_PLACE,
    }

};